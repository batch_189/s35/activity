const express = require('express');
const mongoose = require('mongoose');

const {request, response} = require('express');
const dotenv = require('dotenv');
dotenv.config();

const app = express();
const port = 3001;
// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));


mongoose.connect(`mongodb://127.0.0.1:27017/s35-activity`, 
// mongoose.connect(`mongodb+srv://michella:${process.env.MONGODB_PASSWORD}@cluster0.6wklp.mongodb.net/?retryWrites=true&w=majority`, 
    {
        useNewUrlParser : true,
        useUnifiedTopology : true
    }
);

let db = mongoose.connection

db.on('error', () => console.error('Connection Error :<'));
db.on('open', () => console.log('Connection to MongoDB! :>'));

// SCHEMA
const userSchema = new mongoose.Schema({
    username: String,
    password: String
});

// Model
const User = mongoose.model('User', userSchema);

// Creating Route
app.post('/signup', (request, response) => {
    
    User.findOne({username: request.body.username}, (error, result) => {
        
        if (result != null && result.username == request.body.username){
            return response.send('Duplicate User')
        } else {
            let newUser = new User ({
                username : request.body.username,
                password : request.body.password
            });

            newUser.save((error, savedUser) => {

                if (error) {
                    return console.error(error)
                } else {
                    return response.send('New user Registered')
                }
            }); 
        };
    });
});

app.listen(port, () => console.log(`server is running at port ${port}`));


